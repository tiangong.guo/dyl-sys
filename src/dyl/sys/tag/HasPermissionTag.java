package dyl.sys.tag;

import java.math.BigDecimal;

import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import dyl.common.util.AuthUtil;
import dyl.common.util.Constants;
import dyl.sys.Annotation.AuthAction;
import dyl.sys.bean.SysUser;

/**
 * @author dyl E-mail:84829698@qq.com
 * 2017/05/24
 */
public class HasPermissionTag extends TagSupport {
    private static final long serialVersionUID = 1L;
    private String kind;//菜单子
    private int menuId;//菜单id
    public int getMenuId() {
		return menuId;
	}
    public void setMenuId(int menuId) {
		this.menuId = menuId;
	}
    public String getKind() {
		return kind;
	}
    public void setKind(String kind) {
		this.kind = kind;
	}
    public int doStartTag() throws JspException {
        try {
            return isPermitted();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return TagSupport.SKIP_BODY;
    }
    
    public int isPermitted() throws Exception{
        boolean show = true;
        HttpSession session = pageContext.getSession(); 
    	SysUser u  = (SysUser)session.getAttribute(Constants.SESSION_USER_KEY);
    	if(u.isAdmin())return TagSupport.EVAL_BODY_INCLUDE;
    	BigDecimal kindId = null;
    	for (AuthAction auth :AuthAction.values()) {
			if(auth.toString().equals(kind)){
				kindId = new BigDecimal(auth.getIndex());
			}
		}
    	if(!AuthUtil.hasPermission(new BigDecimal(menuId),kindId,u.getMenuAuthMap())){//判断是否有权限
    		show = false;
		}
        if (show) {
            return TagSupport.EVAL_BODY_INCLUDE;
        } else {
            return TagSupport.SKIP_BODY;
        }
    }
}